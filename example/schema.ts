import { DatabaseSchema } from '../src/fauna/domain/schema/model/DatabaseSchema'

const schema: DatabaseSchema = {
  models: [
    {
      name: 'Model',
      searchFields: ['integrationRef', 'name'],
      sortFields: ['createdAt', 'name'],
      multiSort: false,
      uniqueFields: [['uniqueId']],
    },
  ],
}

export default schema
