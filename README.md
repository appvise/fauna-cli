# Fauna CLI

## Description
[Fauna-CLI](https://github.com/UberSaas/Fauna-CLI) CLI for managing [Fauna](https://www.fauna.com) databases.

## Usage
Create an .env file with your Fauna secret set to the FAUNADB_SERVER_SECRET environment variable.
```bash
npm run install
npm run build
npm run fauna <command> [options]
```

## Commands
```bash
test                                                                                                        Test action
list                                                                                                        List everything in current database
list:databases                                                                                              List all databases
create:databases [names]                                                                                    Create one or more databases
delete:databases [options] [names]                                                                          Delete one or more databases
delete:databases:all [options]                                                                              Delete all databases
sweep [options]                                                                                             Sweep database. Deletes everything inside the database: child databases, collections, indexes, functions, roles.
list:collections                                                                                            List all collections
create:collections [names]                                                                                  Create one or more collections
delete:collections [options] [names]                                                                        Delete one or more collections
delete:collections:all [options]                                                                            Delete all collections
list:indexes                                                                                                List all indexes
create:index [name] [collectionName] [unique] [terms] [values] [reverse]                                    Create an index
create:geohash-index [name] [collectionName] [path] [minPrefixLength] [maxPrefixLength] [values] [reverse]  Create a geohash index
create:indexes:search [collectionName] [searchFields] [requiredFilter]                                      Create search indexes for a collection
create:indexes:sort [collectionName] [sortFields] [multiSort]                                               Create sort indexes for a collection
create:indexes:unique [collectionName] [uniqueFields]                                                       Create unique indexes for a collection with one or more fields like: field1,field2;set2Field1,set2Field2
delete:indexes [options] [names]                                                                            Delete one or more indexes
delete:indexes:all [options]                                                                                Delete all indexes
create:model [name] [requiredFilter] [searchFields] [sortFields] [multiSort]                                Setup collection + search and sort indexes for a model
create:schema [fileName]                                                                                    Setup a database from a configuration file
help [command]                                                                                              display help for command
```