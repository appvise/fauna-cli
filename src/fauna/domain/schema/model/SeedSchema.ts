export interface SeedDocument {
  model: string
  data: Record<string, unknown>
  children?: Array<SeedChildDocument>
}

export interface SeedChildDocument extends SeedDocument {
  parentRefName: string
}

export interface SeedSchema {
  documents: Array<SeedDocument>
}
