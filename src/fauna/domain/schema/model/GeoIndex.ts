export interface GeoIndex {
  name: string
  collectionName: string,
  path: string[],
  minPrefixLength: number,
  maxPrefixLength: number,
  values?: string[][],
  reverse?: boolean[]
}
