export interface SchemaIndex {
  name: string
  terms?: string[] | string[][]
  values?: string[][]
  unique?: boolean
  sortable?: boolean
}
