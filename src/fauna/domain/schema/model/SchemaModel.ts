import { SchemaIndex } from './SchemaIndex'
import { GeoIndex } from './GeoIndex'

export interface SchemaModel {
  name: string
  indexes?: Array<SchemaIndex>
  geohashIndexes?: Array<GeoIndex>,
  requiredFilter?: string
  searchFields?: Array<string>
  sortFields?: Array<string>
  uniqueFields?: Array<Array<string>>
  multiSort?: boolean
}
