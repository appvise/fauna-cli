import * as faunadb from 'faunadb'
import { ClientConfig } from 'faunadb/src/types/Client'

export class FaunaService {
  getClient(): faunadb.Client {
    if (process.env.FAUNADB_SERVER_SECRET == null) {
      throw Error('No FAUNADB_SERVER_SECRET')
    }

    const config: ClientConfig = {
      secret: process.env.FAUNADB_SERVER_SECRET,
    }

    if (process.env.FAUNADB_DOMAIN) {
      config.domain = process.env.FAUNADB_DOMAIN
    }

    if (process.env.FAUNADB_SCHEME != null &&
        (process.env.FAUNADB_SCHEME === 'http' || process.env.FAUNADB_SCHEME === 'https')
    ) {
      config.scheme = process.env.FAUNADB_SCHEME
    }

    if (process.env.FAUNADB_PORT) {
      config.port = parseInt(process.env.FAUNADB_PORT)
    }

    return new faunadb.Client(config)
  }
}
