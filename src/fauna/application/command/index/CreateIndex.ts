import * as faunadb from 'faunadb'
const q = faunadb.query
import { AbstractCommand } from '../AbstractCommand'

export class CreateIndex extends AbstractCommand {
  public execute(
    name: string,
    collectionName: string,
    unique?: boolean,
    terms?: string[] | string[][],
    values?: string[][],
    reverse?: boolean[]
  ): Promise<any> {
    let termsInput: any = undefined
    let valuesInput: any = undefined

    if (terms !== undefined) {
      termsInput = []

      terms.forEach((term: string | string[]) => {
        termsInput.push({
          field: term
        })
      })
    }

    if (values !== undefined && values.length > 0) {
      valuesInput = []

      let hasCustomRef = false
      values.forEach((value, index) => {
        if (value.includes('ref')) {
          hasCustomRef = true
        }

        valuesInput.push({
          field: value,
          reverse:
            reverse !== undefined && reverse[index] != null
              ? reverse[index]
              : undefined,
        })
      })

      if (!hasCustomRef) {
        // Add ref as last value if not added by user
        valuesInput.push({field: ['ref']})
      }
    }

    return this.query(
      q.Do(
        q.CreateIndex({
          name,
          source: q.Collection(collectionName),
          unique: unique !== undefined ? unique : false,
          terms: termsInput,
          values: valuesInput,
        })
      )
    )
  }
}
