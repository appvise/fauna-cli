import * as faunadb from 'faunadb'
const q = faunadb.query
import { AbstractCommand } from '../AbstractCommand'

export class CreateGeohashIndex extends AbstractCommand {
  /**
   *
   * @param name
   * @param collectionName
   * @param path Path for the geohash field (exclude data)
   * @param minPrefixLength
   * @param maxPrefixLength Max “prefix” is 32 and is the entire geohash: 4/030202112231011311302131301203
   * @param values
   * @param reverse
   */
  public execute(
    name: string,
    collectionName: string,
    path: string[],
    minPrefixLength: number,
    maxPrefixLength: number,
    values?: string[][],
    reverse?: boolean[]
  ): Promise<any> {
    if (maxPrefixLength > 32) {
      maxPrefixLength = 32
    }

    let valuesInput: any = undefined

    if (values !== undefined && values.length > 0) {
      valuesInput = []

        let hasCustomRef = false

        values.forEach((value, index) => {
          if (value.includes('ref')) {
            hasCustomRef = true
          }

          valuesInput.push({
            field: value,
            reverse:
              reverse !== undefined && reverse[index] != null
                ? reverse[index]
                : undefined,
          })
      })

      if (!hasCustomRef) {
        // Add ref as last value if not added by user
        valuesInput.push({field: ['ref']})
      }
    }

    // A range of integers from 3 to 32
    const allPossiblePrefixLengths = Array.from(
      { length: maxPrefixLength - minPrefixLength + 1 },
      (_, i) => minPrefixLength + i
    )

    return this.query(
      q.Do(
        q.CreateIndex({
          name: name,
          source: {
            collection: q.Collection(collectionName),
            fields: {
              prefixes: q.Query(
                q.Lambda(
                  'row',
                  q.Let(
                    {
                      geohash: q.Select(path, q.Var('row')),
                      prefixLengths: q.Take(
                        q.Subtract(
                          q.Length(q.Var('geohash')),
                          minPrefixLength - 1
                        ),
                        allPossiblePrefixLengths
                      ),
                    },
                    q.Map(
                      q.Var('prefixLengths'),
                      q.Lambda((prefixLength) =>
                        q.SubString(q.Var('geohash'), 0, prefixLength)
                      )
                    )
                  )
                )
              ),
            },
          },
          // Terms are what we search on
          terms: [{ binding: 'prefixes' }],
          values: valuesInput,
        })
      )
    )
  }
}
