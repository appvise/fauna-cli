import * as faunadb from 'faunadb'
const q = faunadb.query
import { AbstractCommand } from '../AbstractCommand'

export class CreateDocument extends AbstractCommand {
    public execute(collectionName: string, data: Record<string, unknown>): Promise<any> {
        return this.query(
            q.Do(
                q.Create(
                    q.Collection(collectionName),
                    { data }
                )
            )
        )
    }
}
