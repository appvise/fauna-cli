import * as chalk from 'chalk'
import { Input } from '@nestjs/cli/commands'
import { AbstractAction } from '@nestjs/cli/actions'
import * as ora from 'ora'
import { CreateGeohashIndex } from '../../../fauna/application/command/index/CreateGeohashIndex'

export class CreateGeohashIndexAction extends AbstractAction {
  public async handle(inputs: Input[], options: Input[]): Promise<void> {
    const spinner = ora().start('Create geohash index')

    const nameInput = inputs.find((value: Input) => {
      return value.name === 'name'
    })

    const collectionNameInput = inputs.find((value: Input) => {
      return value.name === 'collectionName'
    })

    const pathInput = inputs.find((value: Input) => {
      return value.name === 'path'
    })

    const minPrefixLengthInput = inputs.find((value: Input) => {
      return value.name === 'minPrefixLength'
    })

    const maxPrefixLengthInput = inputs.find((value: Input) => {
      return value.name === 'maxPrefixLength'
    })

    const valuesInput = inputs.find((value: Input) => {
      return value.name === 'values'
    })

    const reverseInput = inputs.find((value: Input) => {
      return value.name === 'reverse'
    })

    if (nameInput == null || typeof nameInput.value !== 'string') {
      throw new Error('Incorrect name')
    }

    if (
      collectionNameInput == null ||
      typeof collectionNameInput.value !== 'string'
    ) {
      throw new Error('Incorrect collectionName')
    }

    if (pathInput == null || typeof pathInput.value !== 'string') {
      throw new Error('Incorrect path')
    }

    const indexName = nameInput.value
    const collectionName = collectionNameInput.value
    const path = pathInput.value.split(',')
    const minPrefixLength =
      minPrefixLengthInput !== undefined &&
      typeof minPrefixLengthInput.value === 'string'
        ? parseInt(minPrefixLengthInput.value)
        : 3
    const maxPrefixLength =
      maxPrefixLengthInput !== undefined &&
      typeof maxPrefixLengthInput.value === 'string'
        ? parseInt(maxPrefixLengthInput.value)
        : 32
    const values =
      valuesInput !== undefined && typeof valuesInput.value === 'string'
        ? valuesInput.value.split(',')
        : undefined

    // Map each value to array. Use dots to separate the values: data.myfield.subField
    const valuesArray = values ? values.map(value => value.split('.')) : undefined

    const reverseStrings: undefined | string[] =
      reverseInput !== undefined && typeof reverseInput.value === 'string'
        ? reverseInput.value.split(',')
        : undefined
    const reverse: boolean[] = []

    reverseStrings?.forEach((reverseString) => {
      reverse.push(reverseString === 'true')
    })

    const subAction =
      options && options.find((option) => option.name === 'subAction')

    spinner.info(chalk.cyanBright('\nIndexes created:'))

    await new CreateGeohashIndex()
      .execute(
        indexName,
        collectionName,
        path,
        minPrefixLength,
        maxPrefixLength,
        valuesArray,
        reverse
      )
      .then(() => {
        spinner.info(chalk.yellowBright(`- ${indexName}`))
      })
      .catch((error: any) => {
        spinner.fail(
          chalk.redBright(`Geohash-index '${indexName}' could not be created`)
        )
        spinner.fail(chalk.redBright(error.message))
      })

    spinner.succeed(chalk.green('Action create:geohash-index completed'))

    if (!subAction) {
      process.exit(0)
    }
  }
}
