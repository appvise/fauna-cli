import * as chalk from 'chalk'
import { Input } from '@nestjs/cli/commands'
import { AbstractAction } from '@nestjs/cli/actions'
import * as ora from 'ora'
import { CreateIndex } from '../../../fauna/application/command/index/CreateIndex'

export class CreateUniqueIndexesAction extends AbstractAction {
  public async handle(inputs: Input[], options: Input[]): Promise<void> {
    const spinner = ora().start('Create search indexes')

    const collectionNameInput = inputs.find((value: Input) => {
      return value.name === 'collectionName'
    })

    const uniqueFieldsInput = inputs.find((value: Input) => {
      return value.name === 'uniqueFields'
    })

    if (
      collectionNameInput == null ||
      typeof collectionNameInput.value !== 'string'
    ) {
      throw new Error('Incorrect collectionName')
    }

    if (
      uniqueFieldsInput == null ||
      typeof uniqueFieldsInput.value !== 'string'
    ) {
      throw new Error('Incorrect uniqueFields')
    }

    const collectionName = collectionNameInput.value
    const uniqueFieldSets = uniqueFieldsInput.value.split(';')

    const subAction =
      options && options.find((option) => option.name === 'subAction')

    spinner.info(chalk.cyanBright('\nUnique indexes created:'))

    for (const uniqueFieldSet of uniqueFieldSets) {
      const uniqueFields = uniqueFieldSet.split(',')

      let indexName = collectionName + 'SearchBy'
      let fieldIndex = 0

      const terms = []

      for (const uniqueField of uniqueFields) {
        indexName +=
          (fieldIndex > 0 ? 'And' : '') +
          uniqueField.charAt(0).toUpperCase() +
          uniqueField.slice(1)

        // Field index always comes from data object so prefix it with data.
        terms.push(['data', uniqueField])

        fieldIndex++
      }

      await new CreateIndex()
        .execute(indexName, collectionName, true, terms)
        .then(() => {
          spinner.info(chalk.yellowBright(`- ${indexName}`))
        })
        .catch((error: any) => {
          spinner.fail(
            chalk.redBright(`Unique index '${indexName}' could not be created`)
          )
          spinner.fail(chalk.redBright(error.message))
        })
    }

    spinner.succeed(chalk.green('Action create:indexes:unique completed'))

    if (!subAction) {
      process.exit(0)
    }
  }
}
