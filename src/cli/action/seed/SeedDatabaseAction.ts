import * as chalk from 'chalk'
import { Input } from '@nestjs/cli/commands'
import { AbstractAction } from '@nestjs/cli/actions'
import * as ora from 'ora'
import { SeedChildDocument, SeedDocument, SeedSchema } from '../../../fauna/domain/schema/model/SeedSchema'
import { CreateDocument } from '../../../fauna/application/command/seed/CreateDocument'
import { Expr } from 'faunadb'

export class SeedDatabaseAction extends AbstractAction {
  public async handle(inputs: Input[], options: Input[]): Promise<void> {
    const spinner = ora().start('Import data')

    // Make sure subActions aren't exited
    options = options === undefined ? [] : options
    options.push({ name: 'subAction', value: true })

    const fileName = inputs.find((value: Input) => {
      return value.name === 'fileName'
    })

    if (fileName == null || typeof fileName.value !== 'string') {
      throw new Error('Incorrect fileName')
    }

    let schema: SeedSchema

    const fs = require('fs');
    const path = require('path');

    try {
      let rawdata = fs.readFileSync(path.resolve(fileName.value));
      schema = JSON.parse(rawdata);
    } catch(error) {
      throw new Error('Incorrect file')
    }

    if (schema.documents == null || !Array.isArray(schema.documents)) {
      throw new Error('Incorrect schema')
    }

    spinner.info(chalk.cyanBright('\nDocuments created:'))

    for (const document of schema.documents) {
      await this.createDocument(spinner, document)
    }

    spinner.succeed(chalk.green('Action seed completed'))

    process.exit(0)
  }

  private createDocument(spinner: any, document: SeedDocument) {
    return new CreateDocument()
        .execute(document.model, document.data)
        .then(async (result) => {
          spinner.info(chalk.yellowBright(`- ${result.ref}`))

          if (document.children != null) {
            for (const childDocument of document.children) {
              await this.createChildDocument(spinner, childDocument, result.ref)
            }
          }
        })
        .catch((error: any) => {
          spinner.fail(
              chalk.redBright(`Document of type '${document.model}' could not be created`)
          )
          spinner.fail(chalk.redBright(error.message))
        })
  }

  private createChildDocument(spinner: any, document: SeedChildDocument, parentRef?: Expr) {
    const data = document.data

    if (parentRef && document.parentRefName != null) {
      data[document.parentRefName] = parentRef
    }

    return new CreateDocument()
        .execute(document.model, data)
        .then(async (result) => {
          spinner.info(chalk.yellowBright(`- Child ${result.ref}`))

          if (document.children != null) {
            for (const childDocument of document.children) {
              await this.createChildDocument(spinner, childDocument, result.ref)
            }
          }
        })
        .catch((error: any) => {
          spinner.fail(
              chalk.redBright(`Document of type '${document.model}' could not be created`)
          )
          spinner.fail(chalk.redBright(error.message))
        })
  }
}
