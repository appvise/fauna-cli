import { CommanderStatic } from 'commander'
import { AbstractCommand } from '@nestjs/cli/commands/abstract.command'
import { Input } from '@nestjs/cli/commands'

export class CreateGeohashIndexCommand extends AbstractCommand {
  // @ts-ignore
  load(program: CommanderStatic): void {
    program
      .command(
        'create:geohash-index [name] [collectionName] [path] [minPrefixLength] [maxPrefixLength] [values] [reverse]'
      )
      .description('Create a geohash index')
      .action(
        async (
          name: string,
          collectionName: string,
          path: string,
          minPrefixLength?: number,
          maxPrefixLength?: number,
          values?: string,
          reverse?: string
        ) => {
          const inputs: Input[] = [
            { name: 'name', value: name },
            { name: 'collectionName', value: collectionName },
            {
              name: 'path',
              value: path,
            },
            {
              name: 'minPrefixLength',
              value:
                minPrefixLength !== undefined
                  ? minPrefixLength.toString()
                  : '3',
            },
            {
              name: 'maxPrefixLength',
              value:
                maxPrefixLength !== undefined
                  ? maxPrefixLength.toString()
                  : '32',
            },
            {
              name: 'values',
              value: values !== undefined && values !== 'none' ? values : false,
            },
            {
              name: 'reverse',
              value:
                reverse !== undefined && reverse !== 'none' ? reverse : false,
            },
          ]

          await this.action.handle(inputs)
        }
      )
  }
}
