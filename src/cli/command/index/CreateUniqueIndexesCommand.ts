import { CommanderStatic } from 'commander'
import { AbstractCommand } from '@nestjs/cli/commands/abstract.command'
import { Input } from '@nestjs/cli/commands'

export class CreateUniqueIndexesCommand extends AbstractCommand {
  // @ts-ignore
  load(program: CommanderStatic): void {
    program
      .command('create:indexes:unique [collectionName] [uniqueFields]')
      .description(
        'Create unique indexes for a collection with one or more fields like: field1,field2;set2Field1,set2Field2'
      )
      .action(async (collectionName: string, uniqueFields: string) => {
        const inputs: Input[] = [
          { name: 'collectionName', value: collectionName },
          {
            name: 'uniqueFields',
            value: uniqueFields,
          },
        ]

        await this.action.handle(inputs)
      })
  }
}
