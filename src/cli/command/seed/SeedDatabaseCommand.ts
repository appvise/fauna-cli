import { CommanderStatic } from 'commander'
import { AbstractCommand } from '@nestjs/cli/commands/abstract.command'
import { Input } from '@nestjs/cli/commands'

export class SeedDatabaseCommand extends AbstractCommand {
  // @ts-ignore
  load(program: CommanderStatic): void {
    program
      .command('seed [fileName]')
      .description('Seed a database from a json file')
      .action(async (fileName: string) => {
        const inputs: Input[] = []
        inputs.push({ name: 'fileName', value: fileName })
        await this.action.handle(inputs)
      })
  }
}
